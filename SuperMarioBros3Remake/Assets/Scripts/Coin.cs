﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class Coin : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource  audioCoin;
    private SpriteRenderer playerRender;
    void Start()
    {
        audioCoin = GetComponent<AudioSource>();
        playerRender = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            audioCoin.Play(); // tocar o somda moeda
            playerRender.enabled = false; //deixar o objeto invisivel
            Destroy(gameObject, 1); //destruir o objeto depois de 1 segundo
        }
    }
}
