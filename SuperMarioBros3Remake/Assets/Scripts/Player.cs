﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Speed;
    public float jumpForce;
    public bool isJumping;
    public bool doubleJump;
    // Start is called before the first frame update
    private Rigidbody2D rig;
    private Animator anim;
    private AudioSource audioJump;
    
   

    void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        audioJump = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        down();
    }

    void Move()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f); 
        transform.position += movement * Time.deltaTime * Speed;

        if(Input.GetAxis("Horizontal") > 0)
        {
            anim.SetBool("walk", true);
            transform.eulerAngles = new Vector3(0f, 0f, 0f); //nos permiti rotacionar o objeto
        }
        if(Input.GetAxis("Horizontal") < 0)
        {
            anim.SetBool("walk", true);
            transform.eulerAngles = new Vector3(0f, 180f, 0f); //nos permiti rotacionar o objeto
        }
        if(Input.GetAxis("Horizontal") == 0)
        {
            anim.SetBool("walk", false);
        }
        

    }

    void Jump()
    {
        if(Input.GetButtonDown("Jump"))
        {
            if(!isJumping)
            {
                rig.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
                doubleJump = true;
                anim.SetBool("jump", true);

                //som do pulo
               audioJump.Play();
               
            }
            else
            {
                if(doubleJump)
                {
                    rig.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
                    doubleJump = false;
                    audioJump.Play();                }
            }
            
        }
    }

    void down()
    {
        if(Input.GetAxis("Vertical") < 0)
        {
            anim.SetBool("down", true); 
        }
        else
        {
            anim.SetBool("down", false);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 8)
        {
            isJumping = false; //o personagem não ta pulando,ele ta tocando o chao
            anim.SetBool("jump", false);
        }
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.layer == 8)
        {
            isJumping = true; // o personagem nao ta mais tocando o chao,ele ta pulando
        }
    }

    
}
